package android.ilsurih.ru.task4.receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.ilsurih.ru.task4.MainActivity;
import android.support.v4.app.NotificationCompat;

public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                new Intent(context, MainActivity.class), 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.mContentText = "on boot";
        builder.mContentTitle = "notification on boot";
        builder.setSmallIcon(android.support.v7.appcompat.R.drawable.abc_btn_radio_material);
        Notification notification = builder.build();
        notification.contentIntent = pendingIntent;
        NotificationManager systemService = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        systemService.notify(null, 2, notification);

//        NotificationBuilder.notify(context, R.drawable.ic_info_black_24dp, R.string.app_name,
//                R.string.boot, context.getResources().getInteger(R.integer.boot), true, pendingIntent);
//
    }
}
