package android.ilsurih.ru.task4;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.ilsurih.ru.task4.receivers.WifiReceiver;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TimePicker;

import java.util.Date;

public class MainActivity extends AppCompatActivity {

    public static final int NOW = 1;
    public static final String TAG_MESSAGE = "tag_wifi";
    private WifiReceiver mWifiReceiver;

    private String wifiName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mWifiReceiver = new WifiReceiver();
        findViewById(R.id.now).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotificationCompat.Builder builder = new NotificationCompat.Builder(MainActivity.this);
                builder.mContentText = "now";
                builder.mContentTitle = "notification from button";
                builder.setSmallIcon(android.support.v7.appcompat.R.drawable.abc_btn_radio_material);
                Notification notification = builder.build();
                NotificationManager systemService = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                systemService.notify(null, NOW, notification);
            }
        });

        findViewById(R.id.wifi).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText ed = new EditText(MainActivity.this);
                ed.setHint("wifi name");
                if (wifiName != null)
                    ed.setText(wifiName);
                new AlertDialog.Builder(MainActivity.this)
                        .setView(ed)
                        .setPositiveButton("start monitoring", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startMonitoring(String.valueOf(ed.getText()));
                            }
                        })
                        .setNeutralButton("stop monitoring", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                stopMonitoring(String.valueOf(ed.getText()));
                            }
                        }).create().show();
            }
        });

        findViewById(R.id.time).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        setTimeNotification(hourOfDay, minute);
                    }
                }, new Date().getHours(), new Date().getMinutes(), true).show();
            }
        });
    }

    private void setTimeNotification(int hourOfDay, int minute) {

    }

    private void stopMonitoring(String name) {
        if (mWifiReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mWifiReceiver);
        }
    }

    private void startMonitoring(String name) {
        if (name != null && !name.equals("")) {
            LocalBroadcastManager.getInstance(this).registerReceiver(mWifiReceiver, new IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION));
            Intent intent = new Intent(WifiManager.NETWORK_STATE_CHANGED_ACTION);
            intent.putExtra(TAG_MESSAGE, name);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }
    }
}
