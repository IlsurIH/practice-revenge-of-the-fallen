package ru.ilsurih.android.task1.util;

import android.content.ContentValues;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ru.ilsurih.android.task1.model.Point;

public class Utils {

    private static final String JSON_RESPONSE = "response";
    private static final String JSON_GEO_OBJECT_COLLECTION = "GeoObjectCollection";
    private static final String JSON_FEATURE_MEMBER = "featureMember";
    private static final String JSON_GEO_OBJECT = "GeoObject";
    private static final String JSON_NAME = "name";
    private static final String JSON_POINT = "Point";
    private static final String JSON_POS = "pos";

    public static List<Point> parse(String json) {
        List<Point> places = new ArrayList<>();

        JSONObject jsonObject;
        JSONArray placesJson;
        try {
            jsonObject = new JSONObject(json);
            placesJson = jsonObject.getJSONObject(JSON_RESPONSE).getJSONObject(JSON_GEO_OBJECT_COLLECTION)
                    .getJSONArray(JSON_FEATURE_MEMBER);
        } catch (Exception e) {
            Log.e("ParserUtils", "error", e);
            return places;
        }

        for (int i = 0; i < placesJson.length(); i++) {

            try {
                JSONObject geoObject = placesJson.getJSONObject(i).getJSONObject(JSON_GEO_OBJECT);
                JSONObject point = geoObject.getJSONObject(JSON_POINT);
                String[] coordinates = point.getString(JSON_POS).split(" ");
                places.add(
                        new Point(new LatLng(
                                Double.parseDouble(coordinates[1]),
                                Double.parseDouble(coordinates[0])), geoObject.getString(JSON_NAME)));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return places;
    }

    public static List<LatLng> asArray(List<Point> data) {
        List<LatLng> latLngs = new ArrayList<>(data.size());
        for (Point p : data) {
            latLngs.add(p.getPosition());
        }
        return latLngs;
    }

    public static ContentValues[] asContentValuesArray(List<Point> data) {
        ContentValues [] latLngs = new ContentValues[data.size()];

        for (int i = 0 ; i < data.size(); i++) {
            Point point = data.get(i);
            latLngs[i] = point.asContentValues();
        }
        return latLngs;
    }
}
