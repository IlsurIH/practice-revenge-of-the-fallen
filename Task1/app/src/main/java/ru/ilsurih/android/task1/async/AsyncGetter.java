package ru.ilsurih.android.task1.async;

import android.content.AsyncTaskLoader;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.List;

import ru.ilsurih.android.task1.model.Point;
import ru.ilsurih.android.task1.model.PointProvider;
import ru.ilsurih.android.task1.util.Utils;

public class AsyncGetter extends AsyncTaskLoader<List<Point>> {

    private static final String REQUEST = "https://geocode-maps.yandex.ru/1.x/?geocode=%s,%s&kind=locality&rspn=0&format=json&sco=latlong";
    private final LatLng current;

    public AsyncGetter(Context context, LatLng current) {
        super(context);
        this.current = current;
    }

    @Override
    public List<Point> loadInBackground() {

        String url = String.format(REQUEST, current.latitude, current.longitude);

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .build();

        try {
            Response response = client.newCall(request).execute();
            ContentResolver contentResolver = getContext().getContentResolver();
            contentResolver.delete(PointProvider.CONTACT_CONTENT_URI, null, null);
            List<Point> points = Utils.parse(response.body().string());
            ContentValues[] vals = Utils.asContentValuesArray(points);
            contentResolver.bulkInsert(PointProvider.CONTACT_CONTENT_URI, vals);
            return points;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
