package ru.ilsurih.android.task1;

import android.app.LoaderManager;
import android.content.DialogInterface;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;

import com.directions.route.Route;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import ru.ilsurih.android.task1.async.AsyncGetter;
import ru.ilsurih.android.task1.model.Point;
import ru.ilsurih.android.task1.model.PointProvider;
import ru.ilsurih.android.task1.util.Utils;


public class MapsActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback, LoaderManager.LoaderCallbacks<List<Point>>, RoutingListener {

    private static final int LOADER_NEAREST_POINTS = 1;
    private GoogleApiClient mGoogleApiClient;
    private GoogleMap mGoogleMap;
    private LatLng currentLatLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Location mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mCurrentLocation != null) {
            double currentLatitude = mCurrentLocation.getLatitude();
            double currentLongitude = mCurrentLocation.getLongitude();

            currentLatLng = new LatLng(currentLatitude, currentLongitude);
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 13));
            pointMe();
            addClosestPlaces(new LatLng(currentLatitude, currentLongitude));
        }
    }

    private void pointMe() {
        mGoogleMap.addMarker(new MarkerOptions()
                .position(currentLatLng)
                .title(getResources().getString(R.string.current_position))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
    }

    private void addClosestPlaces(LatLng lantng) {
        Bundle bndl = new Bundle();
        bndl.putParcelable("latng", lantng);
        getLoaderManager().initLoader(LOADER_NEAREST_POINTS, bndl, this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(final Marker marker) {

                final EditText ed = new EditText(MapsActivity.this);
                ed.setText(marker.getSnippet());
                ed.setHint("Input description here");
                new AlertDialog.Builder(MapsActivity.this)
                        .setView(ed)
                        .setCancelable(true)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Point point = new Point(marker.getPosition(), marker.getTitle());
                                point.setDescription(String.valueOf(ed.getText()));
                                getContentResolver().update(
                                        PointProvider.CONTACT_CONTENT_URI,
                                        point.asContentValues(),
                                        PointProvider.POINT_LAT + "=? and "
                                                + PointProvider.POINT_LON + "=? ",
                                        new String[]{
                                                String.valueOf(marker.getPosition().latitude),
                                                String.valueOf(marker.getPosition().longitude),
                                        }
                                );
                                marker.setSnippet(String.valueOf(ed.getText()));
                            }
                        })
                        .setNeutralButton("Remove", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getContentResolver().delete(
                                        PointProvider.CONTACT_CONTENT_URI,
                                        PointProvider.POINT_LAT + "=? and "
                                                + PointProvider.POINT_LON + "=? ",
                                        new String[]{
                                                String.valueOf(marker.getPosition().latitude),
                                                String.valueOf(marker.getPosition().longitude),
                                        }
                                );
                                updateMap();
                            }
                        })
                        .create().show();
            }
        });
        lookupDatabase();
        buildGoogleApiClient();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        } else {
            Log.e("Main", "Connection Failed");
        }
    }

    private void updateMap() {
        mGoogleMap.clear();
        pointMe();
        List<LatLng> latLngs = lookupDatabase();
        createWay(latLngs);
    }

    private List<LatLng> lookupDatabase() {
        List<LatLng> latLngs = new ArrayList<>(10);
        Cursor cursor = getContentResolver().query(PointProvider.CONTACT_CONTENT_URI, null, null, null, null);
        if (cursor.moveToFirst())
            do {
                LatLng position = new LatLng(
                        cursor.getDouble(2),
                        cursor.getDouble(3)
                );
                mGoogleMap.addMarker(new MarkerOptions()
                        .title(cursor.getString(1))
                        .position(position)
                        .snippet(cursor.getString(4)));
                latLngs.add(position);
            }
            while (cursor.moveToNext());
        return latLngs;
    }

    @Override
    public Loader<List<Point>> onCreateLoader(int id, Bundle args) {
        AsyncGetter getter = new AsyncGetter(this, (LatLng) args.getParcelable("latng"));
        getter.forceLoad();
        return getter;
    }

    @Override
    public void onLoadFinished(Loader<List<Point>> loader, List<Point> data) {
        if (data == null)
            return;
        mGoogleMap.clear();
        pointMe();
        for (Point p : data) {
            mGoogleMap.addMarker(p.asMarker());
        }
        List<LatLng> waypoints = Utils.asArray(data);
        createWay(waypoints);
    }

    private void createWay(List<LatLng> waypoints) {
        Routing routing = new Routing.Builder()
                .travelMode(Routing.TravelMode.WALKING)
                .withListener(this)
                .waypoints(waypoints)
                .build();
        routing.execute();
    }

    @Override
    public void onRoutingSuccess(PolylineOptions polylineOptions, Route route) {
        PolylineOptions polyoptions = new PolylineOptions();
        polyoptions.color(Color.GREEN);
        polyoptions.width(10);
        polyoptions.addAll(polylineOptions.getPoints());
        mGoogleMap.addPolyline(polyoptions);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onRoutingCancelled() {

    }

    @Override
    public void onRoutingFailure() {

    }

    @Override
    public void onLoaderReset(Loader<List<Point>> loader) {

    }

    @Override
    public void onRoutingStart() {

    }
}
