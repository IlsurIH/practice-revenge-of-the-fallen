package ru.ilsurih.android.task1.model;

import android.content.ContentValues;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class Point {
    int id;
    LatLng position;
    String markerName;
    String description;

    public Point(LatLng position, String markerName) {
        this.position = position;
        this.markerName = markerName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMarkerName(String markerName) {
        this.markerName = markerName;
    }

    public void setPosition(LatLng position) {
        this.position = position;
    }

    public LatLng getPosition() {
        return position;
    }

    public String getMarkerName() {
        return markerName;
    }

    public String getDescription() {
        return description;
    }

    public MarkerOptions asMarker() {
        MarkerOptions markerOptions = new MarkerOptions().position(position).title(markerName);
        if (description != null)
            markerOptions.snippet(description);
        return markerOptions;
    }

    public ContentValues asContentValues() {

        ContentValues cv = new ContentValues();
        cv.put(PointProvider.POINT_NAME, getMarkerName());
        cv.put(PointProvider.POINT_LON, getPosition().longitude);
        cv.put(PointProvider.POINT_LAT, getPosition().latitude);
        cv.put(PointProvider.POINT_DESCRIPTION, getDescription());
        return cv;
    }
}
