package ru.ilsurih.android.task2;

import android.app.Application;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.File;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ImageLoader.getInstance().init(
                new ImageLoaderConfiguration.Builder(this)
                        .diskCache(new UnlimitedDiskCache(
                                new File(App.this.getCacheDir().toString() + "cache.tmp")
                        ))
                        .diskCacheFileCount(40).memoryCacheSize(512 * 1024 * 20).build()
        );
    }
}
