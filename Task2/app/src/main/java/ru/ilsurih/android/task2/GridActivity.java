package ru.ilsurih.android.task2;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.widget.ImageView;

import com.kogitune.activity_transition.fragment.FragmentTransitionLauncher;

public class GridActivity extends AppCompatActivity implements MyAdapter.OnItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid);
        Toolbar bar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(bar);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (ft.isEmpty()) {
            ListFragment fragment = new ListFragment();
            fragment.setOnItemClickListener(this);
            ft.add(R.id.container, fragment);
        }
        else {
            ft.attach(getSupportFragmentManager().getFragments().get(0));
        }
        ft.commit();
    }

    @Override
    public void onItemClick(ImageView imageView) {
        ListFragment imagesListFragment = (ListFragment) getSupportFragmentManager().getFragments().get(0);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Bitmap image = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        DetailFragment imageFragment = new DetailFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable( "image", image);
        imageFragment.setArguments(bundle);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            prepareTransition(imagesListFragment, imageFragment);
            String string = getString(R.string.transition);
            imageView.setTransitionName(string);
            transaction.addSharedElement(imageView, string);
        } else {
            FragmentTransitionLauncher.with(imageView.getContext()).image(image).from(imageView).prepare(imageFragment);
        }
        transaction.replace(R.id.container, imageFragment).addToBackStack(null).commit();

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void prepareTransition(ListFragment imagesListFragment, DetailFragment imageFragment) {
        Transition changeTransform = TransitionInflater.from(this)
                .inflateTransition(R.transition.transition);
        Transition explodeTransition = TransitionInflater.from(this)
                .inflateTransition(android.R.transition.explode);

        imagesListFragment.setSharedElementReturnTransition(changeTransform);
        imagesListFragment.setExitTransition(explodeTransition);

        imageFragment.setSharedElementEnterTransition(changeTransform);
        imageFragment.setEnterTransition(explodeTransition);
    }
}
