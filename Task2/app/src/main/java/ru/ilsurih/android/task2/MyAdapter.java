package ru.ilsurih.android.task2;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.Holder> {

    Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(ImageView view);
    }

    public MyAdapter(Context ctx) {
        this.ctx = ctx;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    @Override
    public MyAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(new ImageView(ctx));
    }

    @Override
    public void onBindViewHolder(MyAdapter.Holder holder, int position) {
        ImageLoader.getInstance().displayImage(images[position], holder.iv);
    }

    @Override
    public int getItemCount() {
        return images.length;
    }

    class Holder extends RecyclerView.ViewHolder{

        ImageView iv;

        public Holder(View itemView) {
            super(itemView);
            iv = (ImageView) itemView;
            iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickListener.onItemClick(iv);
                }
            });
        }
    }

    private final static String[] images = new String[]{
            ("http://marketingland.com/wp-content/ml-loads/2012/01/android_logo-1.gif")
            , ("http://samsung-galaxy3.com/wp-content/uploads/2013/01/9368_132941336415.jpg")
            , ("http://www.google.ru/intl/ru/mobile/android/images/android.jpg")
            , ("http://mobinfo.kz/wp-content/uploads/2014/06/android_logo.png")
            , ("http://www.siliconrus.com/wp-content/uploads/2014/02/android-640x480-wallpaper-889.jpg")
            , ("http://www.computerra.ru/wp-content/uploads/2013/08/Android-Army-1.jpg")
            , ("http://www.iphones.ru/wp-content/uploads/2012/02/01-2-Android5.jpg")
            , ("http://www-static.se-mc.com/blogs.dir/0/files/2015/03/sonymobile-xperia-software-major-01-header-4000px-66013a147af741bcc35ed2fe6231a1e2-940.jpg")
            , ("http://www.3dnews.ru/assets/external/illustrations/2012/12/26/639591/androidwallpaper.jpg")
            , ("http://picpulp.com/wp-content/uploads/2013/03/android_wallpaper_by_kubines-d48mixu.jpg")

            , ("http://www.stroydodyr.ru/files/pervaya1.jpg")
            , ("http://www.1366x768.ru/illusion/7/illusion-wallpaper-1366x768.jpg")
            , ("http://img12.nnm.me/e/a/4/1/5/ea415692686786b290e95cf8e86f5c02_full.jpg")
            , ("http://www.1366x768.ru/illusion/16/illusion-wallpaper-1366x768.jpg")
            , ("http://img1.liveinternet.ru/images/attach/c/0//63/240/63240531_onihitode2.jpg")
            , ("https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSZNNVk1BU4MyH8JdixzjXa52MEGXZADapc04TXJdmqWsEj1snJ6w")
            , ("http://img0.joyreactor.cc/pics/post/%25D0%25BE%25D0%25BF%25D1%2582%25D0%25B8%25D1%2587%25D0%25B5%25D1%2581%25D0%25BA%25D0%25B8%25D0%25B5-%25D0%25B8%25D0%25BB%25D0%25BB%25D1%258E%25D0%25B7%25D0%25B8%25D0%25B8-%25D0%25BF%25D0%25B5%25D1%2581%25D0%25BE%25D1%2587%25D0%25BD%25D0%25B8%25D1%2586%25D0%25B0-496576.jpeg")
            , ("https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQbf1-CXP_DpSqG8pvQrOsLv3c96mT6XNkH0A0XyBIkvPfrtMdV")
            , ("http://illjuzija.ru/wp-content/uploads/2011/05/illyuziya-vrashhayushhixsya-diskov.jpg")
            , ("http://illuzi.ru/files/images/rotbeankakenplastic2.preview.jpg")

            , ("http://mcomp.org/wp-content/uploads/2015/03/foto-kosmos-19.jpg")
            , ("http://4tololo.ru/files/images/201324101801154389.jpeg")
            , ("http://oformi.net/uploads/gallery/main/245/1920-1200-14.jpg")
            , ("http://dokkuziklim.net/wp-content/uploads/2014/07/isiq.jpg")
            , ("http://statusi3.ru/wp-content/uploads/2013/05/1920-1200-21.jpg")
            , ("http://art-print.by/wp-content/uploads/gallery/kosmos/17space10.jpg")
            , ("http://www.zastavki.com/pictures/originals/2014/Space_Spiral_space_080593_.jpg")
            , ("http://mirgif.com/KARTINKI/kosmos/kosmos-94.jpg")
            , ("http://img2.goodfon.ru/original/1920x1080/e/bb/kosmos-galaktika-vselennaya.jpg")
            , ("http://unnatural.ru/wp-content/uploads/2014/01/a4.jpeg")
    };
}
