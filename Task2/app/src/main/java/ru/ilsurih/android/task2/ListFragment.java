package ru.ilsurih.android.task2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ListFragment extends Fragment {
    MyAdapter adapter;
    private MyAdapter.OnItemClickListener listener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        RecyclerView list = (RecyclerView) view.findViewById(R.id.list);
        adapter = new MyAdapter(getActivity());
        adapter.setOnItemClickListener(listener);
        list.setAdapter(adapter);
        list.setLayoutManager(new StaggeredGridLayoutManager(
                getResources().getInteger(R.integer.numColumns),
                StaggeredGridLayoutManager.VERTICAL
        ));

        return view;
    }

    public void setOnItemClickListener(MyAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }
}
