package ru.ilsurih.android.task2;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class DetailFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        ImageView image = (ImageView) view.findViewById(R.id.fragment_image);
        image.setImageBitmap((Bitmap) getArguments().getParcelable("image"));
        return view;
    }
}
